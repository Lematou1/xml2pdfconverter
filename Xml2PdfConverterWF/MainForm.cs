﻿using System;
using System.Windows.Forms;

namespace Xml2PdfConverterWF
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            pageSizeCombo.SelectedIndex = 0;
        }


        private static Converter converter = new Converter();

        private void convertButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(converter.pdfPath) || String.IsNullOrEmpty(converter.xmlPath))
            {
                MessageBox.Show("Сначала выберите место сохранения", "Предупреждение");
                return;
            }
            
            converter.htmlPath = converter.pdfPath.Replace("pdf", "html");
            try
            {
                converter.ConvertXml2Html(converter.xmlPath, converter.htmlPath);
                converter.Convert2Pdf(converter.htmlPath,
                    converter.pdfPath,
                    (float)(Convert.ToDouble(zoomBox.Text) / 100),
                    pageSizeCombo.SelectedIndex + 1,
                    (int)topMargin.Value * 10,
                    (int)bottomMargin.Value * 10,
                    (int)leftMargin.Value * 10,
                    (int)rightMargin.Value * 10);
                if (openPdfCheckBox.CheckState == CheckState.Checked) System.Diagnostics.Process.Start(converter.pdfPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при обработке файла. Original error: " + ex.Message + "\n StackTrace: " + ex.StackTrace, "Ошибка");
            }
            finally
            {
                //converter.pdfPath = null;
                //converter.htmlPath = null;
            }
            
        }

        

        private void openFileButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            converter.xmlPath = null;
            converter.pdfPath = null;
            converter.htmlPath = null;
            openFileDialog.InitialDirectory = Environment.CurrentDirectory;
            openFileDialog.Filter = "xml файл (*.xml)|*.xml";
            openFileDialog.FilterIndex = 1;
            openFileDialog.RestoreDirectory = true;

            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    converter.xmlPath = openFileDialog.FileName;
                    fileNameLabel.Text = converter.xmlPath;
                    pdfNameBox.Text = converter.xmlPath.Substring(converter.xmlPath.LastIndexOf(@"\")+1,
                        converter.xmlPath.LastIndexOf('.') - converter.xmlPath.LastIndexOf(@"\")-1);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Ошибка чтения файла. Original error: " + ex.Message + "\n StackTrace: " + ex.StackTrace, "Ошибка");
                }
            }
        }

        private void saveToButton_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(converter.xmlPath)) { MessageBox.Show("Сначала выберите файл", "Предупреждение"); return; }

            using (var dialog = new FolderBrowserDialog())
            {
                dialog.SelectedPath = converter.xmlPath.Substring(0,converter.xmlPath.LastIndexOf(@"\") + 1);
                if (dialog.ShowDialog() == DialogResult.OK)
                    converter.pdfPath = dialog.SelectedPath + @"\" + (pdfNameBox.Text.Contains(".pdf") ? pdfNameBox.Text : pdfNameBox.Text + ".pdf");
            }
        }
    }
}
