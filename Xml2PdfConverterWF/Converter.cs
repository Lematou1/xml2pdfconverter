﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml;
using System.Xml.Xsl;

namespace Xml2PdfConverterWF
{
    class Converter
    {
        internal string xmlPath { get; set; }
        internal string htmlPath { get; set; }
        internal string pdfPath { get; set; }
        public Converter()
        {

        }

        public void ConvertXml2Html(string xmlPath, string htmlPath)
        {
            var xsltSettings = new XsltSettings() { EnableDocumentFunction = true };
            var xDoc = new XmlDocument();
            var settings = new XmlReaderSettings();
            var reader = XmlReader.Create(xmlPath, settings);

            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;
            settings.XmlResolver = new XmlUrlResolver();

            xDoc.Load(reader);

            var cssUrls = (from XmlNode childNode in xDoc.ChildNodes
                           where childNode.NodeType == XmlNodeType.ProcessingInstruction && childNode.Name == "xml-stylesheet"
                           select (XmlProcessingInstruction)childNode
                           into procNode
                           select Regex.Match(procNode.Data, "href=\"(?<url>.*?)\"").Groups["url"].Value).ToList();
            var myXslTrans = new XslCompiledTransform();


            foreach (var url in cssUrls)
            {
                using (XmlReader xr = XmlReader.Create(url, new XmlReaderSettings() { DtdProcessing = DtdProcessing.Parse }))
                {
                    myXslTrans.Load(xr, xsltSettings, null);

                }
            }
               
            myXslTrans.Transform(xmlPath, htmlPath);
                
        }

      public  void Convert2Pdf(string htmlPath, string pdfPath, float zoom, int pageSize, int topMargin, int bottomMargin, int leftMargin, int rightMargin)
        {
            var bytes = GeneratePdfWithParameters( htmlPath,  zoom,  pageSize,  topMargin,  bottomMargin,  leftMargin,  rightMargin);
            File.WriteAllBytes(pdfPath, bytes);
        }

        

        public byte[] GeneratePdfWithParameters(string htmlPath, float zoom, int pageSize, int topMargin, int bottomMargin, int leftMargin, int rightMargin)
        {
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            htmlToPdf.Margins = new NReco.PdfGenerator.PageMargins()
            {
                Top = topMargin,
                Bottom = bottomMargin,
                Left = leftMargin,
                Right = rightMargin
            };
            htmlToPdf.Zoom = zoom;
            htmlToPdf.Size = (NReco.PdfGenerator.PageSize)pageSize;
            var pdfBytes = htmlToPdf.GeneratePdfFromFile(htmlPath, null);
            return pdfBytes;
        }

    }
}
