﻿namespace Xml2PdfConverterWF
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convertButton = new System.Windows.Forms.Button();
            this.pdfNameBox = new System.Windows.Forms.TextBox();
            this.openFileButton = new System.Windows.Forms.Button();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.saveToButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.zoomBox = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.pageSizeCombo = new System.Windows.Forms.ComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.leftMargin = new System.Windows.Forms.NumericUpDown();
            this.rightMargin = new System.Windows.Forms.NumericUpDown();
            this.topMargin = new System.Windows.Forms.NumericUpDown();
            this.bottomMargin = new System.Windows.Forms.NumericUpDown();
            this.openPdfCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topMargin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomMargin)).BeginInit();
            this.SuspendLayout();
            // 
            // convertButton
            // 
            this.convertButton.Location = new System.Drawing.Point(229, 226);
            this.convertButton.Name = "convertButton";
            this.convertButton.Size = new System.Drawing.Size(101, 23);
            this.convertButton.TabIndex = 6;
            this.convertButton.Text = "Конвертировать";
            this.convertButton.UseVisualStyleBackColor = true;
            this.convertButton.Click += new System.EventHandler(this.convertButton_Click);
            // 
            // pdfNameBox
            // 
            this.pdfNameBox.Location = new System.Drawing.Point(199, 48);
            this.pdfNameBox.Name = "pdfNameBox";
            this.pdfNameBox.Size = new System.Drawing.Size(131, 20);
            this.pdfNameBox.TabIndex = 1;
            // 
            // openFileButton
            // 
            this.openFileButton.Location = new System.Drawing.Point(15, 226);
            this.openFileButton.Name = "openFileButton";
            this.openFileButton.Size = new System.Drawing.Size(101, 23);
            this.openFileButton.TabIndex = 4;
            this.openFileButton.Text = "Открыть XML";
            this.openFileButton.UseVisualStyleBackColor = true;
            this.openFileButton.Click += new System.EventHandler(this.openFileButton_Click);
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoEllipsis = true;
            this.fileNameLabel.Location = new System.Drawing.Point(13, 9);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(317, 36);
            this.fileNameLabel.TabIndex = 0;
            this.fileNameLabel.Text = "Выбранный файл";
            // 
            // saveToButton
            // 
            this.saveToButton.Location = new System.Drawing.Point(122, 226);
            this.saveToButton.Name = "saveToButton";
            this.saveToButton.Size = new System.Drawing.Size(101, 23);
            this.saveToButton.TabIndex = 5;
            this.saveToButton.Text = "Куда сохранить";
            this.saveToButton.UseVisualStyleBackColor = true;
            this.saveToButton.Click += new System.EventHandler(this.saveToButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Имя PDF файла";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(177, 100);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Масштаб";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(7, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(39, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Левая";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 52);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(45, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Правая";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(7, 78);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Верхняя";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(7, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 10;
            this.label6.Text = "Нижняя";
            // 
            // zoomBox
            // 
            this.zoomBox.Location = new System.Drawing.Point(236, 97);
            this.zoomBox.Name = "zoomBox";
            this.zoomBox.Size = new System.Drawing.Size(41, 20);
            this.zoomBox.TabIndex = 2;
            this.zoomBox.Text = "100";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.bottomMargin);
            this.groupBox1.Controls.Add(this.topMargin);
            this.groupBox1.Controls.Add(this.rightMargin);
            this.groupBox1.Controls.Add(this.leftMargin);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(12, 74);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(159, 130);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Границы страницы PDF";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(127, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(21, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "cм";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(127, 52);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(21, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "cм";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(127, 78);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(21, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "см";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(127, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 20;
            this.label11.Text = "см";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(283, 100);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(15, 13);
            this.label12.TabIndex = 18;
            this.label12.Text = "%";
            // 
            // pageSizeCombo
            // 
            this.pageSizeCombo.FormattingEnabled = true;
            this.pageSizeCombo.Items.AddRange(new object[] {
            "A4",
            "A3",
            "Letter"});
            this.pageSizeCombo.Location = new System.Drawing.Point(236, 126);
            this.pageSizeCombo.Name = "pageSizeCombo";
            this.pageSizeCombo.Size = new System.Drawing.Size(62, 21);
            this.pageSizeCombo.TabIndex = 3;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(177, 129);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(49, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Формат";
            // 
            // leftMargin
            // 
            this.leftMargin.DecimalPlaces = 1;
            this.leftMargin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.leftMargin.Location = new System.Drawing.Point(73, 24);
            this.leftMargin.Name = "leftMargin";
            this.leftMargin.Size = new System.Drawing.Size(48, 20);
            this.leftMargin.TabIndex = 0;
            this.leftMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // rightMargin
            // 
            this.rightMargin.DecimalPlaces = 1;
            this.rightMargin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.rightMargin.Location = new System.Drawing.Point(73, 50);
            this.rightMargin.Name = "rightMargin";
            this.rightMargin.Size = new System.Drawing.Size(48, 20);
            this.rightMargin.TabIndex = 1;
            this.rightMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // topMargin
            // 
            this.topMargin.DecimalPlaces = 1;
            this.topMargin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.topMargin.Location = new System.Drawing.Point(73, 76);
            this.topMargin.Name = "topMargin";
            this.topMargin.Size = new System.Drawing.Size(48, 20);
            this.topMargin.TabIndex = 2;
            this.topMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // bottomMargin
            // 
            this.bottomMargin.DecimalPlaces = 1;
            this.bottomMargin.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.bottomMargin.Location = new System.Drawing.Point(73, 102);
            this.bottomMargin.Name = "bottomMargin";
            this.bottomMargin.Size = new System.Drawing.Size(48, 20);
            this.bottomMargin.TabIndex = 3;
            this.bottomMargin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // openPdfCheckBox
            // 
            this.openPdfCheckBox.Checked = true;
            this.openPdfCheckBox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.openPdfCheckBox.Location = new System.Drawing.Point(180, 159);
            this.openPdfCheckBox.Name = "openPdfCheckBox";
            this.openPdfCheckBox.Size = new System.Drawing.Size(150, 45);
            this.openPdfCheckBox.TabIndex = 21;
            this.openPdfCheckBox.Text = "Открыть файл после завершения?";
            this.openPdfCheckBox.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AcceptButton = this.convertButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(338, 261);
            this.Controls.Add(this.openPdfCheckBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.pageSizeCombo);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.zoomBox);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.saveToButton);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.openFileButton);
            this.Controls.Add(this.pdfNameBox);
            this.Controls.Add(this.convertButton);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Xml to PDF or HTML";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.leftMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.rightMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topMargin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bottomMargin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button convertButton;
        private System.Windows.Forms.TextBox pdfNameBox;
        private System.Windows.Forms.Button openFileButton;
        private System.Windows.Forms.Label fileNameLabel;
        private System.Windows.Forms.Button saveToButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox zoomBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox pageSizeCombo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.NumericUpDown leftMargin;
        private System.Windows.Forms.NumericUpDown bottomMargin;
        private System.Windows.Forms.NumericUpDown topMargin;
        private System.Windows.Forms.NumericUpDown rightMargin;
        private System.Windows.Forms.CheckBox openPdfCheckBox;
    }
}

